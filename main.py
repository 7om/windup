import sys

from PySide6.QtCore import QPoint, Qt, QTimer
from PySide6.QtGui import (
    QColor,
    QCursor,
    QKeyEvent,
    QMouseEvent,
    QPainter,
    QPaintEvent,
    QPen,
    QResizeEvent,
)
from PySide6.QtWidgets import QApplication, QWidget


class WinDup(QWidget):
    FPS = 10

    def __init__(
        self, parent: QWidget | None = None, f: Qt.WindowType = Qt.WindowType.Window
    ) -> None:
        super().__init__(parent, f)
        self.setWindowTitle("WinDup")

        self.screens = app.screens()
        print(self.screens)
        self.screen = app.primaryScreen()

        self.pos: tuple[int, int] = (0, 0)
        self.size: tuple[int, int] = (1, 1)

        self.pointer_pen = QPen()
        self.pointer_pen.setWidth(5)
        self.pointer_pen.setColor(QColor.fromHsv(0, 255, 255, 255))

        self.mouse_pos_prev: tuple(int, int) | None = None

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.setInterval(1000 // WinDup.FPS)
        self.timer.start()

    def paintEvent(self, _event: QPaintEvent) -> None:
        R = 15
        mouse_pos = QCursor.pos()
        mouse_screen = app.screenAt(mouse_pos)
        pm = self.screen.grabWindow(0, *self.pos, *self.size)
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.drawPixmap(QPoint(0, 0), pm)

        if mouse_screen is self.screen:
            painter.setPen(self.pointer_pen)
            painter.drawEllipse(
                mouse_pos.x() - self.pos[0] - mouse_screen.geometry().left() - R,
                mouse_pos.y() - self.pos[1] - mouse_screen.geometry().top() - R,
                2 * R,
                2 * R,
            )

    def keyPressEvent(self, event: QKeyEvent) -> None:
        D = 20
        match event.key(), event.modifiers():
            case Qt.Key.Key_Left, Qt.KeyboardModifier.NoModifier:
                self.shift_pos(-D, 0)
            case Qt.Key.Key_Right, Qt.KeyboardModifier.NoModifier:
                self.shift_pos(D, 0)
            case Qt.Key.Key_Up, Qt.KeyboardModifier.NoModifier:
                self.shift_pos(0, -D)
            case Qt.Key.Key_Down, Qt.KeyboardModifier.NoModifier:
                self.shift_pos(0, D)
            case Qt.Key.Key_T, Qt.KeyboardModifier.NoModifier:
                self.setWindowFlags(self.windowFlags() ^ Qt.WindowType.FramelessWindowHint)
                self.show()
            case Qt.Key.Key_Space, Qt.KeyboardModifier.NoModifier:
                if self.timer.isActive():
                    self.timer.stop()
                else:
                    self.timer.start()
            case Qt.Key.Key_F, Qt.KeyboardModifier.NoModifier:
                if self.isFullScreen():
                    self.showNormal()
                else:
                    self.showFullScreen()
            case (
                screen_nr,
                Qt.KeyboardModifier.NoModifier,
            ) if Qt.Key.Key_0 <= screen_nr <= Qt.Key.Key_9:
                self.screen = self.screens[min(len(self.screens) - 1, screen_nr - Qt.Key.Key_0)]
                self.shift_pos(0, 0)
            case Qt.Key.Key_Q, Qt.KeyboardModifier.NoModifier:
                app.quit()

        self.update()

    def resizeEvent(self, event: QResizeEvent) -> None:
        self.size = event.size().width(), event.size().height()
        self.shift_pos(0, 0)
        return super().resizeEvent(event)

    def mousePressEvent(self, event: QMouseEvent) -> None:
        if event.buttons() == Qt.MouseButton.LeftButton:
            self.mouse_pos_prev = event.position().toTuple()

        return super().mousePressEvent(event)

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        if self.mouse_pos_prev:
            dx = self.mouse_pos_prev[0] - event.position().x()
            dy = self.mouse_pos_prev[1] - event.position().y()
            self.mouse_pos_prev = event.position().toTuple()
            self.shift_pos(dx, dy)
            self.update()

        return super().mousePressEvent(event)

    def mouseReleaseEvent(self, event: QMouseEvent) -> None:
        if event.buttons() == Qt.MouseButton.LeftButton:
            self.mouse_pos_pres = None

        return super().mouseReleaseEvent(event)

    def shift_pos(self, dx, dy):
        screen_size = self.screen.size().toTuple()

        self.pos = (
            min(screen_size[0] - self.size[0], max(0, self.pos[0] + dx)),
            min(screen_size[1] - self.size[1], max(0, self.pos[1] + dy)),
        )


app = QApplication(sys.argv)

windup = WinDup()
windup.setGeometry(100, 100, 800, 600)
windup.show()

app.exec()

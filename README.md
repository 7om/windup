# WinDup


## Description

WinDup mirrors a part of your screen in a separate window. This can be used in several scenarios:

* For a presentation where you project some part of your main screen (e.g. a
  JupyterLab notebook).
* During an online conference where you want to share some part of your screen but not an entire
  window.


## Usage

To change the size of the mirrored area, just change the size of the WinDup window.

To move the mirrored area, you can either use the cursor keys or click into the WinDup window
and drag the area.

Keyboard:
* 0 .. 9: switch between screens
* Cursor keys: pan the mirrored area
* T: toggle window title bar on/off
* F: toggle fullscreen
* Space: pause/unpause mirroring
* Q: quit


## Dependencies

* Python 3.11
* pyside6


## License

WinDup is being released under the MIT license.

## Plans

Re-write application in Rust or Nim to get rid of the Python dependency.
